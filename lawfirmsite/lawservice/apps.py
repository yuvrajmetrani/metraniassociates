from django.apps import AppConfig


class LawserviceConfig(AppConfig):
    name = 'lawservice'
